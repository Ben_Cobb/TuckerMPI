#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=56

its=$1
timing_output_file=$2
mult_output_file=$3
fin_output=$4
> $timing_output_file
for ((i=1; i<=its; i++))
do
	echo $i
	mpirun -n 4 ./TTM --parameter-file ./paramfile.txt 1 1 --timing-output-file $timing_output_file --mult-output-file $mult_output_file
	#sleep 1
done

awk '{sum+= $1} END {avg=sum/NR; print avg}' $timing_output_file >> $fin_output
#mpirun -n 4 ./TTM -parameter-file ./paramfile.txt 100
