#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=56

cores=$1
its=$2
mode=$3
timing_output_file=$4
mult_output_file=$5
mpirun -n $cores ./TTM -parameter-file ./paramfile.txt $its $mode --timing-output-file $timing_output_file --mult-output-file $mult_output_file
