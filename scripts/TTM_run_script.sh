#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=56
mpirun -n 4 ./TTM -parameter-file ./paramfile.txt
